/* 
 * File:   Main.cpp
 * Author: ajc17dru 100199337
 * Description: Main class for creating the collection and running its tasks
 * Last update: 30/04/2019
 */

#include <cstdlib>
#include "Collection.h"
#include "Track.h"
#include "Album.h"
#include "Duration.h"
#include <iostream>
#include <fstream>

using namespace std;

/**
 * Test function to test all different objects and their uses
 */
void test()
{
    //test formatting by creating duration with too many seconds
    Duration d = Duration(0,0,100);
    //test print
    cout << d << endl;
    
    Duration c2 = Duration(0,2,30);
    //test addition
    cout << d+c2 << endl;
    //test subtraction
    cout << c2-d << endl;
    //test casting to seconds (int)
    cout << int(d) << endl;
    
    //create duration, test formatting by passing too many seconds
    Duration d1 = Duration(0,2,70);
    //create track from existing duration
    Track t1 = Track(std::string("test"),d1);
    //create default track
    Track t2 = Track();
    //create track with temporary duration
    Track t3 = Track("Test title", Duration(0,5,10));

    //print tracks
    std::cout << t1 << std::endl;
    std::cout << t2 << std::endl;
    std::cout << t3 << std::endl;
    
    //test print duration of track
    std::cout << t1.getDuration() << std::endl;
    //test print title of track
    std::cout << t1.getTitle() << std::endl;
    
    //create tracks with different methods and album with no vector
    Track t11 = Track("Test 1",Duration(0,3,40));
    std::string title = "Test 2";
    Track t22 = Track(title, Duration(0,5,70));
    Track t33 = Track(t1);
    Track t4 = t2;

    //add tracks to empty vector manually
    Album a = Album("Test title","Test artist");
    a.addTrack(t11);
    a.addTrack(t22);
    a.addTrack(t33);
    a.addTrack(t4);
    
    //create vector and add tracks
    std::vector<Track> v = std::vector<Track>();
    v.push_back(t11);
    v.push_back(t2);
    v.push_back(t3);
    v.push_back(t4);
    
    //create new album with same tracks but constructed with vector
    Album b = Album("Test title 2", "Test artist 2", v);
            
    //print whole album, total duration, longest track, total tracks, artist,
    //title, then each track in album
    std::cout << a << std::endl;
    std::cout << a.totalDuration() << std::endl;
    std::cout << a.longestTrack() << std::endl;
    std::cout << a.totalTracks() << std::endl;
    std::cout << a.getArtist() << std::endl;
    std::cout << a.getTitle() << std::endl;
    
    for(Track t : a.getTracks())
    {
        std::cout << t << std::endl;
    }
    
    //print whole album, total duration, longest track, total tracks, artist,
    //title, then each track in album but for vector constructed album
    std::cout << b << std::endl;
    std::cout << b.totalDuration() << std::endl;
    std::cout << b.longestTrack() << std::endl;
    std::cout << b.totalTracks() << std::endl;
    std::cout << b.getArtist() << std::endl;
    std::cout << b.getTitle() << std::endl;
    
    for(Track t : b.getTracks())
    {
        std::cout << t << std::endl;
    }
    
    a.addTrack(t1);
    a.addTrack(t2);
    a.addTrack(t3);
    a.addTrack(t4);
    
    b.addTrack(t1);
    b.addTrack(t2);
    b.addTrack(t3);
    b.addTrack(t4);
    
    Collection collection = Collection();
    
    Album *a1 = &a;
    Album *b1 = &b;
    
    collection.addAlbum(a1);
    collection.addAlbum(b1);
    std::cout << collection << std::endl;
}

/** ##########################################################################
 * Currently the program works in all ways except for reading in a albums. That
 * part is commented out to prevent errors. The only other part that does not 
 * work in the sorting function. I could not find the error it caused. I have
 * provided some test functions in main to output to demonstrate that the task
 * function do work correctly, they just cannot be used on the full collection
 * as I am unable to read it in fully. 
 ############################################################################*/
int main(int argc, char** argv) 
{
  //  test();
    
    filebuf fb;
    if(fb.open("albums.txt",ios::in))
    {
        istream is(&fb);
        Collection c = Collection();
        
        Album a = Album("test album", "Pink Floyd");
        a.addTrack(Track("test track",Duration(0,0,100)));
        a.addTrack(Track("test track2",Duration(0,0,30)));
        Album *b = &a;
        c.addAlbum(b);
        
        //reading in the collection does not work as reading in albums does not
        //work. Duration and Track do work when reading in.
       // is >> c;
        c.sortCollection();
        cout << c << endl;
        cout << "Longest album: " << c.longestAlbum()->getTitle() << endl;
        cout << "Total playtime for Pink Floyd: " << c.totalPinkFloyd() << endl;
        cout << "Longest track: " << c.longestTrack() << endl;
    }
    
    fb.close();
    return 0;
}

