/* 
 * File:   Track.h
 * Author: ajc17dru 100199337
 * Description: Header file for Track class for in line methods
 * Last update: 30/04/2019
 */

#ifndef TRACK_H
#define TRACK_H
#include "Duration.h"
#include <string>

class Track 
{
    
private:
    
    std::string title;
    Duration duration;
    
public:
    
    /**
     * Constructor for a Track. Title defaults to "No title found." and duration
     * to 00:00:00
     * @param s String to sue as title
     * @param d
     */
    Track(const std::string s, const Duration& d);
    
    /**
     * Returns the string title of the Track
     * @return string title of the Track
     */
    std::string getTitle() const;
    
    /**
     * Returns the Duration object of this Track
     * @return Duration object in this Track
     */
    Duration getDuration() const;
    
    /**
     * Copy assignment operator
     * @param t Track to assign self to
     * @return new Track
     */
    inline Track& operator =(const Track& t)
    {
        duration = t.getDuration();
        title = t.getTitle();
        return *this;
    }
    
    /**
     * copy constructor
     * @param t Track to copy and assign self to
     */
    inline Track(const Track& t)
    {
        duration = t.getDuration();
        title.assign(t.getTitle());
    }
    
};

inline Track::Track(const std::string s = "No title found", 
        const Duration& d = Duration(0,0,0))
{
    title = s;
    duration = d;
}

inline std::string Track::getTitle() const
{
    return title;
}

inline Duration Track::getDuration() const
{
    return this->duration;
}

inline bool operator == (const Track& t, const Track& s)
{
    return t.getDuration() == s.getDuration();
}

inline bool operator != (const Track& t, const Track& s)
{
    return !(t == s);
}

inline bool operator > (const Track& t, const Track& s)
{
    return t.getDuration() > s.getDuration();
}

inline bool operator >= (const Track& t, const  Track& s)
{
    return t > s || t == s;
}

inline bool operator < (const Track& t, const Track& s)
{
    return !(t > s);
}

inline bool operator <= (const Track& t, const Track& s)
{
    return t < s || t == s;
}

inline Duration operator + (const Track& t, const Track& s)
{
    return t.getDuration() + s.getDuration();
}

inline Duration operator - (const Track& t, const Track& s)
{
    return t.getDuration() - s.getDuration();
}

inline std::ostream& operator<<(std::ostream& str, const Track& t)
{
    return str << t.getTitle() << " - " << t.getDuration();
}

std::istream& operator>>(std::istream& is, Track& t);

#endif /* TRACK_H */
