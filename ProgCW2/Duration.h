/* 
 * File:   Duration.h
 * Author: ajc17dru 100199337
 * Description: Header file for Duration class for in line methods
 * Last update: 30/04/2019
 */

#ifndef DURATION_H
#define DURATION_H

#include <cstdlib>
#include <iostream>  
#include <iomanip>
#include <string>


class Duration
{
private:
    
    int seconds,minutes,hours;
    
public:
    
    /**
    * Uses default of 0 if no value specified values are assigned left to right
    * so if 2 values given, h will be 0 etc
    * @param h hours (default 0)
    * @param m minutes (default 0)
    * @param s seconds (default 0)
    */
    Duration(int h,int m,int s);
    
    /**
    * Get seconds of Duration
    * @return seconds
    */
    int getSeconds() const;
    
    /**
    * Get minutes of Duration
    * @return minutes
    */
    int getMinutes() const;
    
    /**
    * Get hours of Duration
    * @return hours
    */
    int getHours() const;
    
    /**
    * Cast Duration to int by returning total number of seconds in Duration
    * @return total number of seconds
    */
    operator int() const;
    
    /**
    * Casts a duration to a string
    * @return string format of a duration
    */
    operator std::string() const;
    
    /**
     * Copy constructor only needs to shallow as only using primitives
     */
    Duration();
    
    /**
     * Copy assignment constructor 
     * @param d Duration to copy
     * @return New copied duration object
     */
    inline Duration& operator =(const Duration& d)
    {
        seconds = d.getSeconds();
        minutes = d.getMinutes();
        hours = d.getHours();

        return *this;
    }
    
    inline Duration operator+(const Duration& d)
    {
        int sec = this->getSeconds() + d.getSeconds();
        int min = this->getMinutes() + d.getMinutes();
        int hour = this->getHours() + d.getHours();

        if(sec > 59)
        {
            while(sec >59)
            {
                sec -=60;
                min++;
            }
        }
        if(min > 59)
        {
            while(min > 59)
            {
                min -= 60;
                hour++;
            }
        }
        
        return Duration(hour,min,sec);
    }
};

inline Duration::Duration(int h,int m,int s)
{   
    seconds = s;
    minutes = m;
    hours = h;

    if(seconds > 59)
    {
        while(seconds >59)
        {
            seconds -=60;
            minutes++;
        }
    }
    if(minutes > 59)
    {
        while(minutes > 59)
        {
            minutes -= 60;
            hours++;
        }
    }
}

inline Duration::Duration(){}

inline int Duration::getSeconds() const
{
    return this->seconds;
}

inline int Duration::getMinutes() const
{
    return this->minutes;
}

inline int Duration::getHours() const
{ 
    return this->hours;
}

inline bool operator ==(const Duration& d, const Duration& f)
{
    return d.getSeconds() ==  f.getSeconds() &&
            d.getMinutes() == f.getMinutes() &&
            d.getHours() == f.getHours();
}

inline bool operator>(const Duration& d, const Duration& f)
{
    return static_cast<int>(d) > static_cast<int>(f);
}

inline bool operator<(const Duration& d, const Duration& f)
{
    return !(d > f);
}

inline bool operator >=(const Duration& d, const Duration& f)
{
    return d > f || d == f;
}

inline bool operator <=(const Duration& d, const Duration& f)
{
   return d < f || d == f;
}

inline bool operator !=(const Duration& d, const Duration& f)
{
    return !(d==f);
}

inline Duration operator-(const Duration& d, const Duration& f)
{   
    //constructor will automatically convert total seconds into proper format
    return Duration(0,0,static_cast<int>(d) - static_cast<int>(f));
}

inline std::ostream& operator<<(std::ostream& str, const Duration& d)
{
    return str << std::setw(2) << std::setfill('0')<<  d.getHours() << ":" 
               << std::setw(2) << std::setfill('0')<< d.getMinutes()<< ":" 
               << std::setw(2) << std::setfill('0')<<d.getSeconds();
}

std::istream& operator>>(std::istream& is, Duration &d);

#endif /* DURATION_H */

