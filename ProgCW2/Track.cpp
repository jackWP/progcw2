/* 
 * File: Track.cpp
 * Author: ajc17dru 100199337
 * Description: Source file for Track and larger methods, and test 
 *              harness
 * Last update: 30/04/2019
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include "Track.h"

std::istream& operator>>(std::istream& is, Track& t)
{
    std::string title;
    Duration d;
    char c1;
    
    if(is >> d >> c1 >> title)
    {
        if(c1 == '-')
        {
            t = Track(title, d);
        }
        else
        {
            is.clear(std::ios::failbit);
        }
    }
    else
    {
        is.clear(std::ios::failbit);
    }
    
    return is;
}