/* 
 * File: Album.cpp
 * Author: ajc17dru 100199337
 * Description: Source file for Album and in line methods
 * Last update: 30/04/2019
 */

#ifndef ALBUM_H
#define ALBUM_H
#include "Track.h"
#include <vector>

class Album 
{
private:
    
    std::string title;
    std::string artist;
    std::vector<Track> tracks;
    
public:
    
    /**
     * Initialise album form existing vector of tracks
     * @param t title
     * @param a artist
     * @param tracks track vector
     */
    Album(const std::string& t, const std::string& a,
        const std::vector<Track>& tracks);
    
    /**
     * Returns albums title
     * @return title string
     */
    std::string getTitle() const;

    /**
     * Returns albums artist
     * @return artist string
     */
    std::string getArtist() const;
    
    /**
     * Returns full track vector 
     * @return 
     */
    std::vector<Track> getTracks() const;
    
    /**
     * Adds track to track vector
     * @param t Track to add
     */
    void addTrack(const Track& t);
    
    /**
     * Gets total duration of tracks in seconds
     * @return total seconds of all tracks in album
     */
    int totalDuration() const;
    
    /**
     * Gets longest Track in album
     * @return longest duration Track
     */
    Track longestTrack() const;
    
    /**
     * Returns number of tracks in album
     * @return number of tracks in album
     */
    int totalTracks() const;
    
    /**
     * Copy assignment
     * @param a album to copy
     * @return New album
     */
    inline Album& operator =(const Album& a)
    {
        title.assign(a.getTitle());
        artist.assign(a.getArtist());
        
        for(std::vector<Track>::iterator i = a.getTracks().begin(); 
                i != a.getTracks().end(); i++)
        {
            tracks.push_back(*i);
        }
        return *this;
    }
    
    /**
     * Copy constructor
     * @param a new album
     */
    inline Album(const Album& a)
    {
        title.assign(a.getTitle());
        artist.assign(a.getArtist());
        tracks = std::vector<Track>(a.getTracks());
    }
    
};

inline Album::Album(const std::string& t = "", const std::string& a = "", 
        const std::vector<Track>& tracks = {})
{
    title.assign(t);
    artist.assign(a);
    this->tracks = std::vector<Track>(tracks);
}

inline std::string Album::getTitle() const
{
    return this->title;
}

inline std::string Album::getArtist() const
{
    return this->artist;
}

inline std::vector<Track> Album::getTracks() const
{
    return this->tracks;
}

inline void Album::addTrack(const Track& t)
{
    tracks.push_back(t);
}

inline int Album::totalDuration() const
{
    Duration d = Duration(0,0,0);

    for(Track t : this->getTracks())
    {
        d = d + t.getDuration();        
    }

    return int(d);
}

inline Track Album::longestTrack() const
{
    Track t = Track();

    for (Track track : tracks)
    {
        if(t.getTitle() == "No title found")
        {
            t = track;
        }
        else
        {
            if(t.getDuration() < track.getDuration())
            {
                t = track;
            }
        }
    }
    return t;
}

inline int Album::totalTracks() const
{
    return tracks.size();
}

inline std::ostream& operator<<(std::ostream& str, const Album& a)
{
    str << a.getArtist() << " : " << a.getTitle() << "\n";
    for(Track t : a.getTracks())
    {
        str << t << "\n";
    }
    return str;
}

inline bool operator == (const Album& a, const Album& b)
{
    std::string s1 = a.getArtist() + a.getTitle();
    std::string s2 = b.getArtist() + b.getTitle();
    return s1 == s2;
}

inline bool operator != (const Album& a, const Album& b)
{
    return !(a == b);
}

inline bool operator > (const Album& a, const Album& b)
{
    std::string s1 = a.getArtist() + a.getTitle();
    std::string s2 = b.getArtist() + b.getTitle();
    return s1.compare(s2);
}

inline bool operator >= (const Album& a, const Album& b)
{
    return a > b || a == b;
}

inline bool operator < (const Album& a, const Album& b)
{
    return !(a > b);
}

inline bool operator <= (const Album& a, const Album& b)
{
    return a < b || a == b;
}

std::istream& operator>>(std::istream& is, Album& a);
#endif /* ALBUM_H */

