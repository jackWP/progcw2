/* 
 * File:   Duration.cpp
 * Author: ajc17dru 100199337
 * Description: Source file for Duration class and larger methods and test 
 *              harness
 * Last update: 30/04/2019
 */

#include "Duration.h"
#include <iostream>
#include <iomanip>
#include <sstream> 


/**
 * Casts Duration to int by returning total number of seconds in Duration
 * @return total seconds in Duration
 */
Duration::operator int() const
{
    return (((hours*60)+minutes)*60)+seconds;
}

std::istream& operator>>(std::istream& is, Duration& d)
{
    int sec,min,hour;
    char c1,c2;
    
    if(is >> hour >> c1 >> min >> c2 >> sec)
    {
        if(c1 == ':' && c2 == ':')
        {
            d = Duration (hour,min,sec);
        }
        else
        {
            is.clear(std::ios::failbit);
        }
    }
    else
    {
        is.clear(std::ios::failbit);
    }
    
    return is;
}