/* 
 * File: Collection.h
 * Author: ajc17dru 100199337
 * Description: Source file for Collection and larger methods, and test 
 *              harness
 * Last update: 30/04/2019
 */

#ifndef COLLECTION_H
#define COLLECTION_H
#include "Album.h"
#include <vector>
#include <algorithm>
#include <string.h>
#include <iostream>

class Collection 
{
private:
    
    std::vector<Album*> collection;
    
public:
 
    /**
     * Constructor does not need to initialise anything
     */
    Collection();
    
    /**
     * Gets current vector of Albums
     * @return Vector of Albums
     */
    std::vector<Album*> getAlbums() const;
    
    /**
     * 
     * Adds an album to the collection
     * @param a Album to add
     */
    void addAlbum( Album *a);
    
    /**
     * Adds an vector of Album pointers 
     * @param a vector<Album*> to add to collection
     */
    void addAlbumVector(const std::vector<Album*> a);
    
    /**
     * Calculates and returns reference to the longest album by duration 
     * @return Reference to longest album
     */
    Album* longestAlbum() const;
    
    /**
     * Finds and returns the longest track in the collection
     * @return 
     */
    Track longestTrack() const;
    
    /**
     * Calculates and returns the duration of all the pink floyd albums
     * @return 
     */
    Duration totalPinkFloyd() const;
    
    /**
     * Sorts the collections albums by artist then title using a lambda 
     */
    void sortCollection() const;
    
    /**
     * Compares two albums
     * @param a First album pointer
     * @param b Seconds Album pointer
     * @return bool result
     */
    bool compareAlbum( Album *a,  Album *b) ;
    
    /**
     * Destructor for collection
     */
   //  ~Collection();
};

inline Collection::Collection(){}

inline std::vector<Album*> Collection::getAlbums() const
{
    return this->collection;
}

inline void Collection::addAlbum(Album *a)
{
    this->collection.push_back(a);
}

inline void Collection::addAlbumVector(const std::vector<Album*> a)
{
    for(Album* ptr : a)
    {
        this->addAlbum(ptr);
    }
}

inline std::ostream& operator<<(std::ostream& str, const Collection& c)
{
    for(Album *a : c.getAlbums())
    {
        str << *a << "\n";
    }
    return str;
}

inline bool compareAlbum( Album *a,  Album *b)
{
    if((*a)< (*b))
    {
        return true;
    }
    else if ((*a) == (*b))
    {
        return false;
    }
    else
    {
        return false;
    }
}

//inline Collection::~Collection()
//{
//    for(auto a : collection)
//    {
//        delete a;
//    }
//}

inline void Collection::sortCollection() const
{
 //   std::sort(collection.begin(),collection.end(),compareAlbum);
}
    
std::istream& operator>>(std::istream& is, Collection& c);
    

#endif /* COLLECTION_H */

