/* 
 * File: Collection.h
 * Author: ajc17dru 100199337
 * Description: Source file for Collection and larger methods, and test 
 *              harness
 * Last update: 30/04/2019
 */

#include "Collection.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

std::istream& operator>>(std::istream& is, Collection& c)
{
    std::string rest;
    Album a;
    bool cont = true;
    
    while(cont)
    {
        if(is >> a >> rest)
        {
            Album *ptr = &a;
            c.addAlbum(ptr);
        }
        else
        {
            cont = false;
        }
    }
    return is;
}

Track Collection::longestTrack() const
{
    Track t = Track();
    
    for(Album *a : this->getAlbums())
    {
        Track track = a->longestTrack();
        if(track.getDuration() > t.getDuration())
        {
            t  = track;
        }
    }
    return t;
}

Album* Collection::longestAlbum() const
{
    Album *a;
    std::vector<Album*> albums = this->getAlbums();
    if(albums.size() != 0)
    {
        a = *(&albums[0]);

        for(Album *album : albums)
        {
            if(album->totalDuration() > a->totalDuration())
            {
                a = *(&album);
            }
        }
        return a;
    }
    return a;
}

Duration Collection::totalPinkFloyd() const
{
    int d = 0;
    
    for(Album *a : collection)
    {
        if(a->getArtist() == "Pink Floyd")
        {
            d = d + a->totalDuration(); 
        }
    }
    return Duration(0,0,d);
}